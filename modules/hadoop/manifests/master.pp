class hadoop::master inherits hadoop::common{
  
  package { "hadoop-hdfs-namenode":
    ensure  => present,
    allow_virtual => false,
    subscribe => Yumrepo["hdp-repo"],
    require => Package["java-1.8.0-openjdk-devel"],
  }
  
  file { '/etc/init.d/hadoop-hdfs-namenode':
    ensure => 'link',
    target => '/usr/hdp/current/hadoop-hdfs-namenode/etc/rc.d/init.d/hadoop-hdfs-namenode',
    subscribe => Package["hadoop-hdfs-namenode"],
  }
  
  file { ["/var/hadoop/hdfs/namenode"]:
    ensure => "directory",
    owner  => "hdfs",
    group  => "hadoop",
    mode   => 750,
    require => [File["/var/hadoop/hdfs"], Package["hadoop-hdfs-namenode"]],
  }
  
  exec { "hadoop-namenode-format":
    command => "hdfs namenode -format",
    path => '/usr/bin:/usr/sbin:/bin',
    user => "hdfs",
    creates => "/var/hadoop/hdfs/namenode/current/VERSION",
    before => Service["hadoop-hdfs-namenode"],
    require => [File["/etc/hadoop/conf/hdfs-site.xml", "/etc/hadoop/conf/core-site.xml", "/etc/hadoop/conf/hadoop-env.sh", "/var/hadoop/hdfs/namenode"], Package["hadoop-hdfs-namenode"]],
  }

  service { "hadoop-hdfs-namenode":
    ensure => "running",
    require => [File["/etc/init.d/hadoop-hdfs-namenode", "/etc/hadoop/conf/hdfs-site.xml", "/etc/hadoop/conf/core-site.xml", "/etc/hadoop/conf/hadoop-env.sh", "/var/hadoop/hdfs/namenode"], Package["hadoop-hdfs-namenode"]],
  }
  
  





  package { "hadoop-yarn-resourcemanager":
    ensure  => present,
    allow_virtual => false,
    subscribe => Yumrepo["hdp-repo"],
    require => Package["java-1.8.0-openjdk-devel"],
  }
  
  # We shouldnt need to do this but there looks to be a copy paste bug in the resourcemanager init script
  file { '/usr/hdp/current/hadoop-yarn':
    ensure => 'link',
    target => '/usr/hdp/current/hadoop-yarn-resourcemanager',
    subscribe => Package["hadoop-yarn-resourcemanager"],
  }
  
  file { '/etc/init.d/hadoop-yarn-resourcemanager':
    ensure => 'link',
    target => '/usr/hdp/current/hadoop-yarn-resourcemanager/etc/rc.d/init.d/hadoop-yarn-resourcemanager',
    subscribe => Package["hadoop-yarn-resourcemanager"],
  }
  
  service { "hadoop-yarn-resourcemanager":
    ensure => "running",
    require => [File["/etc/init.d/hadoop-yarn-resourcemanager", "/etc/hadoop/conf/hdfs-site.xml", "/etc/hadoop/conf/core-site.xml", "/etc/hadoop/conf/hadoop-env.sh", "/etc/hadoop/conf/yarn-site.xml", "/etc/hadoop/conf/yarn-env.sh"], Package["hadoop-yarn-resourcemanager"]],
  }
  

}