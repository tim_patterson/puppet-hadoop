class hadoop::worker inherits hadoop::common{
  
  package { "hadoop-hdfs-datanode":
    ensure  => present,
    allow_virtual => false,
    subscribe => Yumrepo["hdp-repo"],
    require => Package["java-1.8.0-openjdk-devel"],
  }
  
  file { '/etc/init.d/hadoop-hdfs-datanode':
    ensure => 'link',
    target => '/usr/hdp/current/hadoop-hdfs-datanode/etc/rc.d/init.d/hadoop-hdfs-datanode',
    subscribe => Package["hadoop-hdfs-datanode"],
  }
  
  file { ["/var/hadoop/hdfs/data"]:
    ensure => "directory",
    owner  => "hdfs",
    group  => "hadoop",
    mode   => 700,
    require => [File["/var/hadoop/hdfs"], Package["hadoop-hdfs-datanode"]],
  }

  service { "hadoop-hdfs-datanode":
    ensure => "running",
    require => [File["/etc/init.d/hadoop-hdfs-datanode", "/etc/hadoop/conf/hdfs-site.xml", "/etc/hadoop/conf/core-site.xml", "/etc/hadoop/conf/hadoop-env.sh", "/var/hadoop/hdfs/data"], Package["hadoop-hdfs-datanode"]],
  }



  package { "hadoop-yarn-nodemanager":
    ensure  => present,
    allow_virtual => false,
    subscribe => Yumrepo["hdp-repo"],
    require => Package["java-1.8.0-openjdk-devel"],
  }
  
  file { '/etc/init.d/hadoop-yarn-nodemanager':
    ensure => 'link',
    target => '/usr/hdp/current/hadoop-yarn-nodemanager/etc/rc.d/init.d/hadoop-yarn-nodemanager',
    subscribe => Package["hadoop-yarn-nodemanager"],
  }
  
  service { "hadoop-yarn-nodemanager":
    ensure => "running",
    require => [File["/etc/init.d/hadoop-yarn-nodemanager", "/etc/hadoop/conf/hdfs-site.xml", "/etc/hadoop/conf/core-site.xml", "/etc/hadoop/conf/hadoop-env.sh", "/etc/hadoop/conf/yarn-site.xml", "/etc/hadoop/conf/yarn-env.sh"], Package["hadoop-yarn-nodemanager"]],
  }
}