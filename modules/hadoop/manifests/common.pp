class hadoop::common {
  
  yumrepo { "hdp-repo":
    ensure => "present",
    baseurl => "http://public-repo-1.hortonworks.com/HDP/centos6/2.x/updates/2.3.0.0",
    descr => "Hortonworks HDP repo",
    enabled => 1,
    gpgcheck => 0,
  }
  
  package { "java-1.8.0-openjdk-devel" :
    ensure => present,
    allow_virtual => false,
    require => Yumrepo["hdp-repo"],
  }
  
  package { "hadoop":
    ensure  => present,
    allow_virtual => false,
    subscribe => Yumrepo["hdp-repo"],
    require => Package["java-1.8.0-openjdk-devel"],
  }
  
  package { "hadoop-client":
    ensure  => present,
    allow_virtual => false,
    subscribe => Yumrepo["hdp-repo"],
    require => Package["java-1.8.0-openjdk-devel"],
  }
  
  package { "hadoop-yarn":
    ensure  => present,
    allow_virtual => false,
    subscribe => Yumrepo["hdp-repo"],
    require => Package["java-1.8.0-openjdk-devel"],
  }

  package { "hadoop-mapreduce":
    ensure  => present,
    allow_virtual => false,
    subscribe => Yumrepo["hdp-repo"],
    require => Package["java-1.8.0-openjdk-devel"],
  }

  file { "/etc/profile.d/set_java_home.sh":
    ensure => "present",
    content => "export JAVA_HOME=/usr/lib/jvm/java",
  }
  
  file { "/etc/hadoop/conf/hadoop-env.sh":
    source => "puppet:///modules/hadoop/hadoop-env.sh",
    owner  => "root",
    group  => "root",
    mode   => 644,
    require => Package["hadoop"],
  }

  file { "/etc/hadoop/conf/core-site.xml":
    content => template("hadoop/core-site.xml.erb"),
    owner  => "root",
    group  => "root",
    mode   => 644,
    require => Package["hadoop"],
  }

  file { "/etc/hadoop/conf/hdfs-site.xml":
    content => template("hadoop/hdfs-site.xml.erb"),
    owner  => "root",
    group  => "root",
    mode   => 644,
    require => Package["hadoop"],
  }
  
  file { ["/var/hadoop", "/var/hadoop/hdfs", "/var/hadoop/yarn"]:
    ensure => "directory",
    owner  => "root",
    group  => "root",
    mode   => 755,
  }



  file { "/etc/hadoop/conf/yarn-env.sh":
    source => "puppet:///modules/hadoop/yarn-env.sh",
    owner  => "root",
    group  => "root",
    mode   => 644,
    require => Package["hadoop-yarn"],
  }
  
  file { "/etc/hadoop/conf/yarn-site.xml":
    content => template("hadoop/yarn-site.xml.erb"),
    owner  => "root",
    group  => "root",
    mode   => 644,
    require => Package["hadoop-yarn"],
  }
}